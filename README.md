### Tempus Jump Network ###

This is the repository from which Tempus servers pull their plugins directly.

### Thirdparty Plugins ###

* Detect Freeze Glitch 1.0.0 (https://github.com/laurirasanen/detectfreezeglitch)
* Stop Sticky Bounce 1.0.1 (https://github.com/laurirasanen/stopstickybounce)
