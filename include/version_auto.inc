
#if defined _auto_version_included
 #endinput
#endif
#define _auto_version_included

#define SOURCEMOD_V_TAG		"dev"
#define SOURCEMOD_V_REV		4028
#define SOURCEMOD_V_CSET	"de993f2195f6"
#define SOURCEMOD_V_MAJOR	1
#define SOURCEMOD_V_MINOR	5
#define SOURCEMOD_V_RELEASE	4

#define SOURCEMOD_VERSION	"1.5.4-dev+4028"
    