#include <sdktools>
#include <morecolors>

#pragma newdecls required

#define SLOT_1 0
#define SLOT_2 1
#define SLOT_3 2
#define DECAL_1 "decals/painted_circle.vmt"
#define DECAL_2 "tools/toolshint.vmt"
#define DECAL_3 "vgui/achievements/tf_soldier_crouch_rocket_jump.vmt"

char g_decals[][] = {"hud/obj_circle_blue.vmt", "tools/toolshint.vmt", "vgui/achievements/tf_soldier_crouch_rocket_jump.vmt"};


public void OnPluginStart(){
	RegConsoleCmd("sm_spray", PlayerCMD_Spray);
}

public Action PlayerCMD_Spray(int client, int args) {
	if (ClientValid(client)){
		float eyeAngle[3], eyePos[3], hitLoc[3];
	
		GetClientEyePosition(client, eyePos);
		GetClientEyeAngles(client, eyeAngle);
	
		//Fire a test ray to check for distance to target
		Handle testTrace = TR_TraceRayFilterEx(eyePos, eyeAngle, MASK_VISIBLE, RayType_Infinite, hitPlayer);
		if (TR_DidHit(testTrace)){
			//Test if ray hit surface within certain amount of units
			TR_GetEndPosition(hitLoc, testTrace);
			if (GetVectorDistance(eyePos, hitLoc, false) > 2048){
				CPrintToChat(client, "{white}[{lightskyblue}Spray{white}] {white}Couldn't find surface.");
				CloseHandle(testTrace);
				return Plugin_Continue;
			}
		}
		else {
			CPrintToChat(client, "{white}[{lightskyblue}Spray{white}] {white}Couldn't find surface.");
			CloseHandle(testTrace);
			return Plugin_Continue;
		}
        
        CloseHandle(testTrace);
		//Use active weapon to decide which decal to use
		int activeWep = GetEntProp(client, Prop_Send, "m_hActiveWeapon");
		int wep[3];
		wep[0] = GetEntProp(client, Prop_Send, "m_hMyWeapons", 4, SLOT_1);
		wep[1] = GetEntProp(client, Prop_Send, "m_hMyWeapons", 4, SLOT_2);
		wep[2] = GetEntProp(client, Prop_Send, "m_hMyWeapons", 4, SLOT_3);
		
		for (int i = 0; i < 3; i++){
			if (wep[i] == activeWep){
				TE_Spray(hitLoc, PrecacheDecal(g_decals[i]));
                TE_SendToClient(client);
				return Plugin_Handled;
			}
		}	
	}
	return Plugin_Continue;
}

//Functions 

public bool ClientValid(int client){
	if (!IsFakeClient(client) && IsClientConnected(client) && IsClientInGame(client)){
		return true;
	}
	else{
		return false;
	}
}

void TE_Spray(const float sprayOrigin[3], int sprayIndex){	
	TE_Start("BSP Decal"); // "BSP Decal"  Entity Decal World Decal
	TE_WriteVector("m_vecOrigin", sprayOrigin);
	TE_WriteNum("m_nIndex", sprayIndex);
}

public bool hitPlayer(int entity, any data) {
	//If thing hit (entity) is a player return false and ignore
	return entity > MAXPLAYERS;
}