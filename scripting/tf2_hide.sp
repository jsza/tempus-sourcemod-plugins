#include <sourcemod>
#include <sdkhooks>
#include <tf2_stocks>
#include <morecolors>
#include <updater>

#pragma semicolon 1
#pragma newdecls required

#define EF_NODRAW 32
#define UPDATE_URL    "https://bitbucket.org/jsza/tempus-sourcemod-plugins/raw/master/tf2_hide.txt"
#define PLUGIN_VERSION          "1.0.6"

public Plugin myinfo = {
    name        = "TF2 Hide",
    author      = "[GNC] Matt (modified for Tempus)",
    version     = PLUGIN_VERSION,
};

bool      g_bHide[MAXPLAYERS + 1], g_bHooked;
StringMap g_HidableEntities;
int       g_bCanShoot[MAXPLAYERS + 1], g_Team[MAXPLAYERS + 1];

char g_saHidable[][] = {
	"env_rockettrail",
	"env_smoketrail",
	"env_tracer",
	"env_muzzleflash",
	"halloween_souls_pack",
    "obj_sentrygun",
    "obj_dispenser",
    "obj_teleporter",
	"tf_taunt_prop"
};

char g_sSoundHook[][] = {
	"regenerate",
	"ammo_pickup",
	"pain",
	"grenade_jump",
	"fleshbreak"
};

char g_sHideableMatch[][] = {
    "tf_ammo_pack",
    "tf_projectile",
    "weapon",
	"wearable"
};

char g_saBuildings[][] = {
    "obj_sentrygun",
    "obj_dispenser",
    "obj_teleporter"
};


public void OnPluginStart()
{
    CreateConVar("sm_hide_version", PLUGIN_VERSION, "Hide Players Version.",
                 FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
    RegConsoleCmd("sm_hide", cmdHide, "Show/Hide Other Players");
    RegAdminCmd("sm_hide_reload", cmdReload, ADMFLAG_SLAY,
                "Execute if reloading plugin with players on server.");
    HookEvent("player_team", eventChangeTeam);
	
    g_HidableEntities = CreateTrie();

    AddNormalSoundHook(SoundHook);	
    AddTempEntHook("TFExplosion", TEHook);
    AddTempEntHook("TFParticleEffect", TEHook); //Cow Mangler
    AddTempEntHook("Bubbles", TEHook); //BUBBLES 
    AddTempEntHook("TFBlood",TEHook); //Blood
    AddTempEntHook("Smoke", TEHook); //Smoke
    AddTempEntHook("Surface Shatter", TEHook); //MuzzleFlash
    AddTempEntHook("Bubble Trail", TEHook); //Bubble Trail
    AddTempEntHook("Blood Stream", TEHook); //Blood Stream
    AddTempEntHook("Blood Sprite", TEHook); //Blood Sprite
    AddTempEntHook("Dust", TEHook); //Dust Impact
    AddTempEntHook("Impact", TEHook); //Impact
}

public void OnAllPluginsLoaded()
{
    if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }

    RegServerCmd("sm_tempus_plugins_update", cmdPluginUpdate);
}

public Action cmdPluginUpdate(int args)
{
    if (LibraryExists("updater"))
    {
        Updater_ForceUpdate();
    }
}

public int Updater_OnPluginUpdated()
{

    if (LibraryExists("updater"))
    {
        ReloadPlugin();
    }
}

void CheckHooks()
{
    bool bShouldHook = false;

    for (int i = 1; i <= MaxClients; i++)
    {
        if (g_bHide[i])
        {
            bShouldHook = true;
            break;
        }
    }

    // Fake (un)hook because toggling actual hooks will cause server instability.
    g_bHooked = bShouldHook;
}

public Action SoundHook(int clients[64], int &numClients, char sample[PLATFORM_MAX_PATH], int &entity, int &channel, float &volume, int &level, int &pitch, int &flags)
{
    //Thanks to JoinedSenses for this code:
    //-->
	for (int i = 0; i<=sizeof(g_sSoundHook)-1; i++)
    {
        if (StrContains(sample, g_sSoundHook[i], false) != -1)
        {
            return Plugin_Handled;
        }
    }
    //<--
    if (StrEqual(sample, "vo/null.wav"))
    {
        sample = "vo/null.mp3";
        return Plugin_Changed;
    }
    // Ignore non-weapon sounds.
    if (!g_bHooked || !(strncmp(sample, "weapons", 7) == 0 || strncmp(sample[1], "weapons", 7) == 0))
        return Plugin_Continue;
		
    int i, j;

    for (i = 0; i < numClients; i++)
    {
        if (g_bHide[clients[i]])
        {
            // Remove the client from the array.
            for (j = i; j < numClients-1; j++)
            {
                clients[j] = clients[j+1];
            }

            numClients--;
            i--;
        }
    }
    return (numClients > 0) ? Plugin_Changed : Plugin_Stop;
}

public Action TEHook(const char[] te_name, const int[] Players, int numClients, float delay)
{
    return Plugin_Stop;
}

public Action cmdHide(int client, int args)
{
    g_bHide[client] = !g_bHide[client];

    CPrintToChat(client, "{lightskyblue}[Hide]{default} Other players are now {darkseagreen}%s{default}.", g_bHide[client] ? "hidden" : "visible");
    CheckHooks();
}

public void OnClientPostAdminCheck(int client)
{
    g_bCanShoot[client] = GetUserFlagBits(client) & ADMFLAG_GENERIC;
}

public void OnClientDisconnect_Post(int client)
{
    g_bHide[client] = false;
    g_bCanShoot[client] = false;
    CheckHooks();
}

public Action eventChangeTeam(Handle event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
    int team = GetEventInt(event, "team");

    g_Team[client] = team;
}

public void OnEntityCreated(int entity, const char[] classname)
{
    if (StrEqual(classname, "tf_ammo_pack") || StrEqual(classname, "tf_dropped_weapon")) {
        AcceptEntityInput(entity, "kill");
        return;
    }
    for (int i = 0; i < sizeof(g_sHideableMatch); i++)
    {
        if (StrContains(classname, g_sHideableMatch[i], false) != -1){
            SDKHook(entity, SDKHook_Spawn, OnHidableSpawned);
            return;
        }
    }
    for(int i = 0; i < sizeof(g_saHidable); i++)
    {
        if(StrEqual(classname, g_saHidable[i]) && IsValidEdict(entity))
        {
            SDKHook(entity, SDKHook_Spawn, OnHidableSpawned);
            return;
        }
    }
}

public void OnEntityDestroyed(int entity)
{
    char sEntity[10];
    IntToString(entity, sEntity, sizeof(sEntity));

    SDKUnhook(entity, SDKHook_SetTransmit, Hook_Entity_SetTransmit);
    g_HidableEntities.Remove(sEntity);
}

public void OnHidableSpawned(int entity)
{
    bool isBuilding = false;

    char classname[35];
    GetEdictClassname(entity, classname, sizeof(classname));

    for (int i = 0; i < sizeof(g_saBuildings); i++)
    {
        if(StrEqual(classname, g_saBuildings[i]))
        {
            isBuilding = true;
            break;
        }
    }

    int owner;
    if (isBuilding)
        owner = GetEntPropEnt(entity, Prop_Send, "m_hBuilder");
    else
        owner = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");

    if(owner < 1 || owner > MaxClients)
        return;

    char sEntity[10];
    IntToString(entity, sEntity, sizeof(sEntity));

    g_HidableEntities.SetValue(sEntity, owner);
    SDKHook(entity, SDKHook_SetTransmit, Hook_Entity_SetTransmit);
}

public Action Hook_Entity_SetTransmit(int entity, int client)
{
    char sEntity[10];
    IntToString(entity, sEntity, sizeof(sEntity));

    int owner;
    if(!g_HidableEntities.GetValue(sEntity, owner))
        return Plugin_Continue;

    if(owner == client || !g_bHide[client] || g_Team[client] == 1)
        return Plugin_Continue;
    else
        return Plugin_Handled;
}


public Action cmdReload(int client, int args)
{
    for(int i = 1; i <= MaxClients; i++)
    {
        g_bHide[i] = false;
        SDKUnhook(i, SDKHook_SetTransmit, Hook_Client_SetTransmit);
        SDKHook(i, SDKHook_SetTransmit, Hook_Client_SetTransmit);
    }
    ReplyToCommand(client, "\x05[Hide]\x01 Reloaded");
    return Plugin_Handled;
}

public void OnClientPutInServer(int client)
{
    g_bHide[client] = false;
    SDKHook(client, SDKHook_SetTransmit, Hook_Client_SetTransmit);
}

public Action Hook_Client_SetTransmit(int entity, int client)
{
    if (entity != client
        && GetEntityMoveType(entity) == MOVETYPE_NOCLIP
        && g_Team[client] != 1)
        return Plugin_Handled;
    else if (entity == client || !g_bHide[client] || g_Team[client] == 1)
        return Plugin_Continue;
    else
        return Plugin_Handled;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
    if (buttons & IN_ATTACK && GetEntityMoveType(client) == MOVETYPE_NOCLIP
        && !g_bCanShoot[client]) {
        buttons &= ~IN_ATTACK;
        return Plugin_Changed;
    }
    return Plugin_Continue;
}
