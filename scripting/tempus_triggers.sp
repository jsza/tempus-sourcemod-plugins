#include <sourcemod>
#include <sdkhooks>
#include <dhooks>

#pragma semicolon 1
#pragma newdecls required

#define TRIGGER_ENTER_EVENT "tempus_entertrigger"
#define TRIGGER_LEAVE_EVENT "tempus_leavetrigger"


float g_triggerBounds[1024][2][3];
//float g_lastClientOrigin[MAXPLAYERS + 1][3];

int g_triggerIDs[1024];
int g_numTriggers;
int g_clientThinkCount[MAXPLAYERS + 1];

ArrayList g_inTriggers[MAXPLAYERS + 1];

// for reuse in CheckLoop; store triggers which the current player should
// leave/enter
ArrayStack g_leaveTriggers;
ArrayStack g_enterTriggers;

Address g_hTeleport;


public void OnPluginStart() {
    g_leaveTriggers = CreateStack();
    g_enterTriggers = CreateStack();
    GameData data = LoadGameConfigFile("sdktools.games");
    if (!data)
        SetFailState("Missing sdktools gamedata!");

    Address iOffset = data.GetOffset("Teleport");
    delete data;
    if(iOffset == -1)
        SetFailState("Missing Teleport offset!");

    g_hTeleport = DHookCreate(iOffset, HookType_Entity, ReturnType_Void, ThisPointer_CBaseEntity, Hook_Client_PreTeleport);
    if (!g_hTeleport)
        SetFailState("DHooks failed to create Teleport hook.");

    DHookAddParam(g_hTeleport, HookParamType_VectorPtr);
    DHookAddParam(g_hTeleport, HookParamType_ObjectPtr);
    DHookAddParam(g_hTeleport, HookParamType_VectorPtr);

    for (int client = 1; client <= MaxClients; client++) {
        g_inTriggers[client] = CreateArray();
        if (IsClientInGame(client) && !IsFakeClient(client))
            OnClientPutInServer(client);
    }

    HookEvent("tempus_loadzones", OnTempusLoadZones);

    char filepath[256];
    BuildPath(Path_SM, filepath, sizeof(filepath), "../source-python/data/plugins/tempus/triggers.kv");
    PrintToServer("[Tempus Triggers] Loading triggers on plugin start.");
    LoadTriggers(filepath);
}

public void OnPluginEnd() {
    // Force all players to leave triggers
    for (int client = 1; client <= MaxClients; client++) {
        if (IsClientInGame(client) && !IsFakeClient(client)) {
            LeaveAllTriggers(client);
        }
    }
}


public void OnTempusLoadZones(Event event, const char[] name, bool dontBroadcast) {
    char filepath[256];
    event.GetString("filepath", filepath, sizeof(filepath));
    LoadTriggers(filepath);
}


public void LoadTriggers(const char[] filepath) {
    // Force all players to leave triggers
    for (int client = 1; client <= MaxClients; client++) {
        if (IsClientInGame(client) && !IsFakeClient(client)) {
            LeaveAllTriggers(client);
        }
    }

    KeyValues kvTriggers = new KeyValues("triggers");
    kvTriggers.ImportFromFile(filepath);

    if (!kvTriggers.GotoFirstSubKey()) {
        PrintToServer("[Tempus Triggers] No sub key in 'triggers'.");
        delete kvTriggers;
        return;
    }

    g_numTriggers = 0;
    int index = 0;
    do {
        char sectionName[32];
        kvTriggers.GetSectionName(sectionName, sizeof(sectionName));

        float mins[3];
        float maxs[3];

        mins[0] = kvTriggers.GetFloat("mins_x");
        mins[1] = kvTriggers.GetFloat("mins_y");
        mins[2] = kvTriggers.GetFloat("mins_z");
        maxs[0] = kvTriggers.GetFloat("maxs_x");
        maxs[1] = kvTriggers.GetFloat("maxs_y");
        maxs[2] = kvTriggers.GetFloat("maxs_z");

        CopyVector(mins, g_triggerBounds[index][0]);
        CopyVector(maxs, g_triggerBounds[index][1]);

        int triggerID = StringToInt(sectionName);

        g_triggerIDs[index] = triggerID;

        index++;
        g_numTriggers++;
    } while (kvTriggers.GotoNextKey());

    delete kvTriggers;

    PrintToServer("[Tempus Triggers] Loaded triggers successfully.");
}


public void OnClientPutInServer(int client) {
    ArrayList list = g_inTriggers[client];
    list.Clear();
    if (IsClientInGame(client) && !IsFakeClient(client))
        SDKHook(client, SDKHook_PostThinkPost, Hook_Client_PostThinkPost);
        DHookEntity(g_hTeleport, true, client);
        DHookEntity(g_hTeleport, false, client);
}


public Action Hook_Client_PostThinkPost(int client) {
    CheckLoop(client);
    //GetEntPropVector(client, Prop_Data, "m_vecAbsOrigin", g_lastClientOrigin[client]);
    //g_clientThinkCount[client]++;
}


public MRESReturn Hook_Client_PreTeleport(int client, Handle hParams) {
    CheckLoop(client);
    return MRES_Ignored;
}


void CheckLoop(int client) {
    if (!IsPlayerAlive(client)) {
        LeaveAllTriggers(client);
        return;
    }

    float pPos[3];
    float pMins[3];
    float pMaxs[3];

    GetEntPropVector(client, Prop_Data, "m_vecAbsOrigin", pPos);
    GetEntPropVector(client, Prop_Send, "m_vecMins", pMins);
    GetEntPropVector(client, Prop_Send, "m_vecMaxs", pMaxs);

    // Convert local maxs and mins to global
    for (int i = 0; i < 3; i++) {
        pMins[i] += pPos[i];
        pMaxs[i] += pPos[i];
    }

    for(int idx = 0; idx < g_numTriggers; idx++) {
        float tMins[3];
        float tMaxs[3];

        tMins[0] = g_triggerBounds[idx][0][0];
        tMins[1] = g_triggerBounds[idx][0][1];
        tMins[2] = g_triggerBounds[idx][0][2];

        tMaxs[0] = g_triggerBounds[idx][1][0];
        tMaxs[1] = g_triggerBounds[idx][1][1];
        tMaxs[2] = g_triggerBounds[idx][1][2];

        int isInTrigger = g_inTriggers[client].FindValue(idx);

        // AABB/AABB intersection
        if ((tMins[0] <= pMaxs[0] && tMaxs[0] >= pMins[0]) &&
            (tMins[1] <= pMaxs[1] && tMaxs[1] >= pMins[1]) &&
            (tMins[2] <= pMaxs[2] && tMaxs[2] >= pMins[2])) {
            if (isInTrigger == -1)
                g_enterTriggers.Push(idx);
        }
        else if (isInTrigger != -1) {
            g_leaveTriggers.Push(idx);
        }
    }

    while (!g_leaveTriggers.Empty) {
        LeaveTrigger(client, g_leaveTriggers.Pop());
    }

    while (!g_enterTriggers.Empty) {
        EnterTrigger(client, g_enterTriggers.Pop());
    }
}


void EnterTrigger(int client, int triggerIndex) {
    ArrayList triggerList = g_inTriggers[client];
    triggerList.Push(triggerIndex);

    //float fraction = SweepClientTrigger(client, triggerIndex, false);
    //FireTriggerEvent(TRIGGER_ENTER_EVENT, client, triggerIndex, fraction);
    FireTriggerEvent(TRIGGER_ENTER_EVENT, client, triggerIndex);
}


void LeaveTrigger(int client, int triggerIndex) {
    ArrayList triggerList = g_inTriggers[client];
    int arrayIdx = triggerList.FindValue(triggerIndex);
    triggerList.Erase(arrayIdx);

    //float fraction = SweepClientTrigger(client, triggerIndex, true);
    //FireTriggerEvent(TRIGGER_LEAVE_EVENT, client, triggerIndex, fraction);
    FireTriggerEvent(TRIGGER_LEAVE_EVENT, client, triggerIndex);
}


float SweepClientTrigger(int client, int triggerIndex, bool reverse) {
    float pLastPos[3];
    CopyVector(g_lastClientOrigin[client], pLastPos);

    float pNewPos[3];
    GetEntPropVector(client, Prop_Data, "m_vecAbsOrigin", pNewPos);

    float tMins[3];
    float tMaxs[3];

    CopyVector(g_triggerBounds[triggerIndex][0], tMins);
    CopyVector(g_triggerBounds[triggerIndex][1], tMaxs);

    float pMins[3];
    float pMaxs[3];
    float pOtherMins[3];
    float pOtherMaxs[3];
    GetEntPropVector(client, Prop_Send, "m_vecMins", pMins);
    GetEntPropVector(client, Prop_Send, "m_vecMaxs", pMaxs);
    GetEntPropVector(client, Prop_Send, "m_vecMins", pOtherMins);
    GetEntPropVector(client, Prop_Send, "m_vecMaxs", pOtherMaxs);

    // Reverse sweep
    if (reverse) {
        for (int i = 0; i < 3; i++) {
            pMins[i] += pNewPos[i];
            pMaxs[i] += pNewPos[i];
            pOtherMins[i] += pLastPos[i];
            pOtherMaxs[i] += pLastPos[i];
        }
    }
    else {
        for (int i = 0; i < 3; i++) {
            pMins[i] += pLastPos[i];
            pMaxs[i] += pLastPos[i];
            pOtherMins[i] += pNewPos[i];
            pOtherMaxs[i] += pNewPos[i];
        }
    }

    float pVel[3];
    SubtractVectors(pNewPos, pLastPos, pVel);

    if (reverse) { NegateVector(pVel); }

    float fraction = SweepBoxBox(pMins, pMaxs, tMins, tMaxs, pVel);

    if (reverse) { return fraction; }
    return -fraction;
}


void FireTriggerEvent(const char[] name, int client, int triggerIndex) {
    int triggerID = g_triggerIDs[triggerIndex];
    Event event = CreateEvent(name, true);
    event.SetInt("userid", GetClientUserId(client));
    event.SetInt("triggerID", triggerID);
    //event.SetFloat("time",
    //    GetGameTime() + (fraction * GetTickInterval()));
    //               //(g_clientThinkCount[client] + fraction) * GetTickInterval());
    //event.SetFloat("fraction", fraction);
    event.Fire();
}


void LeaveAllTriggers(int client) {
    ArrayList triggerList = g_inTriggers[client].Clone();
    for (int i = 0; i < triggerList.Length; i++) {
        int triggerIndex = GetArrayCell(triggerList, i);
        LeaveTrigger(client, triggerIndex);
    }
    delete triggerList;
}


void CopyVector(const float[3] vectorIn, float[3] vectorOut) {
    for (int i = 0; i < 3; i++) {
        vectorOut[i] = vectorIn[i];
    }
}


float SweepBoxBox(const float[3] aMin, const float[3] aMax, const float[3] bMin, const float[3] bMax, float[3] v)
{
    // Treat b as stationary, so invert v to get relative velocity
    NegateVector(v);

    float hitTime = 0.0;
    float outTime = 1.0;
    float overlapTime;

    // X axis overlap
    if( v[0] < 0 )
    {
        if( bMax[0] < aMin[0] ) return 0.0;
        if( bMax[0] > aMin[0] ) outTime = MinFloat( (aMin[0] - bMax[0]) / v[0], outTime );

        if( aMax[0] < bMin[0] )
        {
            overlapTime = (aMax[0] - bMin[0]) / v[0];
            hitTime = MaxFloat(overlapTime, hitTime);
        }
    }
    else if( v[0] > 0 )
    {
        if( bMin[0] > aMax[0] ) return 0.0;
        if( aMax[0] > bMin[0] ) outTime = MinFloat( (aMax[0] - bMin[0]) / v[0], outTime );

        if( bMax[0] < aMin[0] )
        {
            overlapTime = (aMin[0] - bMax[0]) / v[0];
            hitTime = MaxFloat(overlapTime, hitTime);
        }
    }

    if( hitTime > outTime ) return 0.0;

    //=================================

    // Y axis overlap
    if( v[1] < 0 )
    {
        if( bMax[1] < aMin[1] ) return 0.0;
        if( bMax[1] > aMin[1] ) outTime = MinFloat( (aMin[1] - bMax[1]) / v[1], outTime );

        if( aMax[1] < bMin[1] )
        {
            overlapTime = (aMax[1] - bMin[1]) / v[1];
            hitTime = MaxFloat(overlapTime, hitTime);
        }
    }
    else if( v[1] > 0 )
    {
        if( bMin[1] > aMax[1] ) return 0.0;
        if( aMax[1] > bMin[1] ) outTime = MinFloat( (aMax[1] - bMin[1]) / v[1], outTime );

        if( bMax[1] < aMin[1] )
        {
            overlapTime = (aMin[1] - bMax[1]) / v[1];
            hitTime = MaxFloat(overlapTime, hitTime);
        }
    }

    // Z axis overlap
    if( v[2] < 0 )
    {
        if( bMax[2] < aMin[2] ) return 0.0;
        if( bMax[2] > aMin[2] ) outTime = MinFloat( (aMin[2] - bMax[2]) / v[2], outTime );

        if( aMax[2] < bMin[2] )
        {
            overlapTime = (aMax[2] - bMin[2]) / v[2];
            hitTime = MaxFloat(overlapTime, hitTime);
        }
    }
    else if( v[2] > 0 )
    {
        if( bMin[2] > aMax[2] ) return 0.0;
        if( aMax[2] > bMin[2] ) outTime = MinFloat( (aMax[2] - bMin[2]) / v[2], outTime );

        if( bMax[2] < aMin[2] )
        {
            overlapTime = (aMin[2] - bMax[2]) / v[2];
            hitTime = MaxFloat(overlapTime, hitTime);
        }
    }

    if( hitTime > outTime ) return 0.0;

    return hitTime;
}


float MinFloat(float a, float b) {
    if (b > a) return a;
    else return b;
}


float MaxFloat(float a, float b) {
    if (b < a) return a;
    else return b;
}
