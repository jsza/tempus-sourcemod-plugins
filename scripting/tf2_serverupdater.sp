#include <sourcemod>
#include <sdktools>
#include <steamtools>
#include <updater>

#pragma semicolon 1
#pragma newdecls required

#define UPDATE_URL    "https://bitbucket.org/jsza/tempus-sourcemod-plugins/raw/master/tf2_serverupdater.txt"
#define PLUGIN_VERSION          "1.0.0"

public Plugin myinfo = {
    name        = "TF2 Server Updater",
    author      = "Tempus",
    version     = PLUGIN_VERSION,
};

public void OnAllPluginsLoaded()
{
    if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
    RegServerCmd("sm_tempus_plugins_update", cmdPluginUpdate);
}

public Action cmdPluginUpdate(int args)
{
    if (LibraryExists("updater"))
    {
        Updater_ForceUpdate();
    }
}

public int Updater_OnPluginUpdated()
{

    if (LibraryExists("updater"))
    {
        ReloadPlugin();
    }
}

public Action Steam_RestartRequested()
{
    if(!IsServerPopulated()) { // If there's no clients in the server, go ahead and restart it
        PrintToServer("No players on the server, restarting now");
        ServerCommand("quit");
        return Plugin_Handled;
    }
    else
    {
        return Plugin_Continue;
    }
}

void IsServerPopulated() {
    for(int i = 1; i <= MaxClients; i++) {
        if(IsClientConnected(i) && !IsFakeClient(i)) {
            return true;
        }
    }
    return false;
}
