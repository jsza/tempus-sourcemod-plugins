#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

#define PLUGIN_VERSION "1.2"

// Notify me if I miss a semicolon
#pragma semicolon 1
#pragma newdecls required

// Entity is completely ignored by the client.
// Can cause prediction errors if a player proceeds to collide with it on the server.
// https://developer.valvesoftware.com/wiki/Effects_enum
#define EF_NODRAW 32

int g_Offset_m_fEffects = -1;
bool g_bShowTriggers[MAXPLAYERS+1];

// Used to determine whether to avoid unnecessary SetTransmit hooks
int g_iTransmitCount;

public Plugin myinfo =
{
    name = "Show Triggers (modified for Tempus)",
    author = "ici",
    description = "Make trigger brushes visible.",
    version = PLUGIN_VERSION,
    url = "http://steamcommunity.com/id/1ci"
};

public void OnPluginStart()
{
    g_Offset_m_fEffects = FindSendPropInfo("CBaseEntity", "m_fEffects");

    if (g_Offset_m_fEffects == -1)
        SetFailState("[Show Triggers] Could not find CBaseEntity:m_fEffects");

    CreateConVar("showtriggers_version", PLUGIN_VERSION, "Showtriggers version", FCVAR_NOTIFY|FCVAR_REPLICATED);

    HookEvent("tempus_showtriggers", OnTempusShowTriggers);
}

public Action OnTempusShowTriggers(Handle event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
    bool bState = GetEventBool(event, "state");
    if (!client)
        return Plugin_Handled;

    if (bState != g_bShowTriggers[client]) {
        g_bShowTriggers[client] = bState;
        if (g_bShowTriggers[client]) {
            // Showing trigger brushes.
            ++g_iTransmitCount;
        } else {
            // Stopped showing trigger brushes.
            --g_iTransmitCount;
        }
        transmitTriggers( g_iTransmitCount > 0 );
    }

    return Plugin_Handled;
}

public void OnClientDisconnect_Post(int client)
{
    // Has this player been still using the feature before he left?
    if (g_bShowTriggers[client])
    {
        g_bShowTriggers[client] = false;
        --g_iTransmitCount;
        transmitTriggers( g_iTransmitCount > 0 );
    }
}

public void OnEntityCreated(int entity, const char[] classname) {
    //if (StrEqual(classname, "trigger_teleport")) {
    //}

    if (StrEqual(classname, "func_nogrenades")) {
        SetEntColor(entity, 255, 0, 0);
    }
    else if (StrEqual(classname, "func_regenerate"))
    {
        SetEntColor(entity, 0, 255, 0);
    }
}

// https://forums.alliedmods.net/showthread.php?p=2423363
// https://sm.alliedmods.net/api/index.php?fastload=file&id=4&
// https://developer.valvesoftware.com/wiki/Networking_Entities
// https://github.com/ValveSoftware/source-sdk-2013/blob/master/sp/src/game/server/triggers.cpp#L58
void transmitTriggers(bool transmit)
{
    // Hook only once
    static bool s_bHooked = false;

    // Have we done this before?
    if (s_bHooked == transmit)
        return;

    // Loop through entities
    char sBuffer[20];
    int lastEdictInUse = GetEntityCount();
    for (int entity = MaxClients+1; entity <= lastEdictInUse; ++entity)
    {
        if ( !IsValidEdict(entity) )
            continue;

        // Is this entity a trigger?
        GetEdictClassname(entity, sBuffer, sizeof(sBuffer));

        if (!(StrEqual(sBuffer, "trigger_teleport")
              || StrEqual(sBuffer, "func_nogrenades")
              || StrEqual(sBuffer, "func_regenerate")))
        {
            continue;
        }

        // Is this entity's model a VBSP model?
        GetEntPropString(entity, Prop_Data, "m_ModelName", sBuffer, 2);
        if (sBuffer[0] != '*') {
            // The entity must have been created by a plugin and assigned some random model.
            // Skipping in order to avoid console spam.
            continue;
        }

        // Get flags
        int effectFlags = GetEntData(entity, g_Offset_m_fEffects);
        int edictFlags = GetEdictFlags(entity);

        // Determine whether to transmit or not
        if (transmit) {
            effectFlags &= ~EF_NODRAW;
            edictFlags &= ~FL_EDICT_DONTSEND;
        } else {
            effectFlags |= EF_NODRAW;
            edictFlags |= FL_EDICT_DONTSEND;
        }

        // Apply state changes
        SetEntData(entity, g_Offset_m_fEffects, effectFlags);
        ChangeEdictState(entity, g_Offset_m_fEffects);
        SetEdictFlags(entity, edictFlags);

        // Should we hook?
        if (transmit)
            SDKHook(entity, SDKHook_SetTransmit, Hook_SetTransmit);
        else
            SDKUnhook(entity, SDKHook_SetTransmit, Hook_SetTransmit);
    }
    s_bHooked = transmit;
}

public Action Hook_SetTransmit(int entity, int client)
{
    if (!g_bShowTriggers[client])
    {
        // I will not display myself to this client :(
        return Plugin_Handled;
    }
    return Plugin_Continue;
}

void SetEntColor(int entity, int r, int g, int b)
{
    int m_clrRender = FindSendPropInfo("CBaseEntity", "m_clrRender");

    SetEntProp(entity, Prop_Send, "m_nRenderMode", 1, 1);
    SetEntData(entity, m_clrRender, r, 1, true);
    SetEntData(entity, m_clrRender + 1, g, 1, true);
    SetEntData(entity, m_clrRender + 2, b, 1, true);
    SetEntData(entity, m_clrRender + 3, 255, 1, true);
}
