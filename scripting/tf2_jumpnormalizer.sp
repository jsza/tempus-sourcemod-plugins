#include <sourcemod>
#include <tf2items>
#include <morecolors>
#include <updater>

#pragma semicolon 1
#pragma newdecls required

#define UPDATE_URL    "https://bitbucket.org/jsza/tempus-sourcemod-plugins/raw/master/tf2_jumpnormalizer.txt"
#define PLUGIN_VERSION          "1.0.13"

public Plugin myinfo = {
    name        = "TF2 Jump Normalizer",
    author      = "Tempus",
    version     = PLUGIN_VERSION,
};

ConVar g_Cvar_DemomanChargeDrain;
ConVar g_Cvar_MedicMaxHealthBoost;
ConVar g_Cvar_MedicUberChargeReleaseRate;
ConVar g_Cvar_SoldierWhipSpeed;

ArrayList g_Notified[MAXPLAYERS+1];

public void OnAllPluginsLoaded()
{
    if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
    RegServerCmd("sm_tempus_plugins_update", cmdPluginUpdate);
}

public Action cmdPluginUpdate(int args)
{
    if (LibraryExists("updater"))
    {
        Updater_ForceUpdate();
    }
}

public int Updater_OnPluginUpdated()
{

    if (LibraryExists("updater"))
    {
        ReloadPlugin();
    }
}

public void OnMapStart()
{
    g_Cvar_DemomanChargeDrain = FindConVar("tf_demoman_charge_drain_time");
    g_Cvar_MedicMaxHealthBoost = FindConVar("tf_max_health_boost");
    g_Cvar_MedicUberChargeReleaseRate = FindConVar("weapon_medigun_chargerelease_rate");
    g_Cvar_SoldierWhipSpeed = FindConVar("tf_whip_speed_increase");

    g_Cvar_DemomanChargeDrain.SetInt(0);
    g_Cvar_MedicMaxHealthBoost.SetInt(1);
    g_Cvar_MedicUberChargeReleaseRate.SetInt(0);
    g_Cvar_SoldierWhipSpeed.SetInt(0);
}

public void OnClientConnected(int client)
{
    g_Notified[client] = CreateArray();
}

public void OnClientDisconnect(int client)
{
    ArrayList notified = g_Notified[client];
    if (notified != INVALID_HANDLE)
        CloseHandle(notified);
}

public Action TF2Items_OnGiveNamedItem(int client, char[] classname, int iItemDefinitionIndex, Handle &hItemOverride)
{
    switch (iItemDefinitionIndex)
    {
        // Soldier
        // The Direct Hit | The Beggar's Bazooka | The Black Box | The Liberty Launcher | Rocket Jumper
        case 127, 730, 228, 414, 1104, 237:
        {
            hItemOverride = TF2Items_CreateItem(OVERRIDE_ATTRIBUTES);
            TF2Items_SetNumAttributes(hItemOverride, 0);
        }
        // The Batallion's Backup, The Concheror, Buff Banner, Festive Buff Banner
        case 226, 354, 129, 1001:
        {
            NotifyDisallowed(client, iItemDefinitionIndex);
            return Plugin_Stop; // Can't replace for some reason
        }

        // Demoman
        // Ali Baba's Wee Booties | The Bootlegger
        case 405, 608:
        {
            NotifyDisallowed(client, iItemDefinitionIndex);
            hItemOverride = TF2Items_CreateItem(OVERRIDE_ATTRIBUTES);
            TF2Items_SetNumAttributes(hItemOverride, 0);
        }
        // The Sticky Jumper | The Scottish Resistance
        case 265, 130, 1150:
        {
            hItemOverride = TF2Items_CreateItem(OVERRIDE_ATTRIBUTES|OVERRIDE_CLASSNAME);
            TF2Items_SetNumAttributes(hItemOverride, 0);
            TF2Items_SetClassname(hItemOverride, "tf_weapon_pipebomblauncher");
        }

        // The Splendid Screen | Scottish Resistance | Chargin' Targe
        case 406, 131, 1099:
        {
            NotifyDisallowed(client, iItemDefinitionIndex);
            return Plugin_Stop; // Can't replace for some reason
        }

        // The Loose Cannon | The Iron Bomber
        case 996, 1151:
        {
            NotifyDisallowed(client, iItemDefinitionIndex);
            return Plugin_Stop; // Can't replace for some reason
        }
        // Ullapool Caber
        case 307:
        {
            NotifyDisallowed(client, iItemDefinitionIndex);
            return Plugin_Stop; // Can't replace for some reason
        }
        // Eyelander, Scotsman's Skullcutter
        case 132, 482, 172:
        {
            NotifyDisallowed(client, iItemDefinitionIndex);
            hItemOverride = TF2Items_CreateItem(OVERRIDE_ATTRIBUTES);
            TF2Items_SetNumAttributes(hItemOverride, 0);
        //Base Jumper, Jarate, Festive Jarate, Mad Milk, The Sandman
        }
        case 1101, 58, 1083, 222, 44:
        {
            NotifyDisallowed(client, iItemDefinitionIndex);
            return Plugin_Stop;
        }
        case 1172:
        // Victory Lap Taunt
        {
            NotifyDisallowed(client, iItemDefinitionIndex);
            return Plugin_Stop;
        }
        case 444:
        // The Mantreads
        {
            NotifyDisallowed(client, iItemDefinitionIndex);
            return Plugin_Stop;
        }
        case 5869, 1069, 1070:
        // Jungle Inferno ConTracker, Fancy Spellbook, Spellbook Magazine
        {
            NotifyDisallowedForHidePlugin(client, iItemDefinitionIndex);
            return Plugin_Stop;
        }
        case 775:
        // The Escape Plan
        {
            hItemOverride = TF2Items_CreateItem(OVERRIDE_ATTRIBUTES);
            TF2Items_SetNumAttributes(hItemOverride, 0);
        }
		case 415, 1153:
        // Panic Attack shotgun, The Reserve Shooter
        {
            hItemOverride = TF2Items_CreateItem(OVERRIDE_ATTRIBUTES);
            TF2Items_SetNumAttributes(hItemOverride, 0);
        }
        //Both classes
        case 357 :
        // The Half-Zatoichi
        {
            hItemOverride = TF2Items_CreateItem(OVERRIDE_ATTRIBUTES);
            TF2Items_SetNumAttributes(hItemOverride, 0);
        }
    }

    if (hItemOverride)
    {
        return Plugin_Changed;
    }

    return Plugin_Continue;
}

void GetItemName(int IDI, char name[32])
{
    switch (IDI)
    {
        case 44: Format(name, sizeof(name), "The Sandman");
        case 58: Format(name, sizeof(name), "Jarate");
        case 127: Format(name, sizeof(name), "The Direct Hit");
        case 129: Format(name, sizeof(name), "The Buff Banner");
        case 130: Format(name, sizeof(name), "Scottish Resistance");
        case 131: Format(name, sizeof(name), "Chargin' Targe");
        case 132: Format(name, sizeof(name), "The Eyelander");
        case 228: Format(name, sizeof(name), "The Black Box");
        case 222: Format(name, sizeof(name), "Mad Milk");
        case 226: Format(name, sizeof(name), "The Batallion's Backup");
        case 265: Format(name, sizeof(name), "The Sticky Jumper");
        case 307: Format(name, sizeof(name), "Ullapool Caber");
        case 354: Format(name, sizeof(name), "The Concheror");
        case 405: Format(name, sizeof(name), "Ali Baba's Wee Booties");
        case 406: Format(name, sizeof(name), "The Splendid Screen");
        case 414: Format(name, sizeof(name), "The Liberty Launcher");
        case 482: Format(name, sizeof(name), "Nessie's Nine Iron");
        case 608: Format(name, sizeof(name), "The Bootlegger");
        case 750: Format(name, sizeof(name), "The Beggar's Bazooka");
        case 996: Format(name, sizeof(name), "The Loose Cannon");
        case 1001: Format(name, sizeof(name), "Festive Buff Banner");
        case 1083: Format(name, sizeof(name), "Festive Jarate");
        case 1099: Format(name, sizeof(name), "The Tide Turner");
        case 1101: Format(name, sizeof(name), "The B.A.S.E. Jumper");
        case 1104: Format(name, sizeof(name), "The Air Strike");
        case 1150: Format(name, sizeof(name), "The Quickiebomb Launcher");
        case 1151: Format(name, sizeof(name), "The Iron Bomber");
        case 1172: Format(name, sizeof(name), "The Victory Lap");
        case 444: Format(name, sizeof(name), "The Mantreads");
        case 5869: Format(name, sizeof(name), "Jungle Inferno ConTracker");
        case 1069: Format(name, sizeof(name), "Fancy Spellbook");
        case 1070: Format(name, sizeof(name), "Spellbook Magazine");
        case 775: Format(name, sizeof(name), "The Escape Plan");
        case 357: Format(name, sizeof(name), "The Half-Zatoichi");
		case 172: Format(name, sizeof(name), "The Scotsman's Skullcutter");
		case 1153: Format(name, sizeof(name), "Panic Attack Shotgun");
		case 415: Format(name, sizeof(name), "The Reserve Shooter");
    }
}

void NotifyDisallowed(int client, int IDI)
{
    // Late load
    if (g_Notified[client] == null)
        g_Notified[client] = CreateArray();

    ArrayList notified = g_Notified[client];

    if (notified.FindValue(IDI) != -1)
        return;

    char name[32];
    GetItemName(IDI, name);
    CPrintToChat(client, "{darkseagreen}%s {white}is not allowed.", name);
    PushArrayCell(g_Notified[client], IDI);
}

void NotifyDisallowedForHidePlugin(int client, int IDI)
{
    // Late load
    if (g_Notified[client] == null)
        g_Notified[client] = CreateArray();

    ArrayList notified = g_Notified[client];

    if (notified.FindValue(IDI) != -1)
        return;

    char name[32];
    GetItemName(IDI, name);
    CPrintToChat(client, "{darkseagreen}%s {white}is disabled because it breaks the !hide plugin.", name);
    PushArrayCell(g_Notified[client], IDI);
}
