#include <sourcemod>
#include <smlib>

#pragma newdecls required

public Plugin myinfo =
{
    name = "spectate",
    author = "jayess",
    description = "Advanced spectate command",
    version = "0.1",
    url = "http://www.sourcemod.net",
}

public void OnPluginStart ()
{
    LoadTranslations("common.phrases");
    RegConsoleCmd("sm_spec", Command_Spec);
    RegConsoleCmd("sm_spectate", Command_Spec);
}

public Action Command_Spec (int client, int args)
{
    if (args < 1)
    {
        ForcePlayerSuicide(client);
        ChangeClientTeam(client, 1);
        return Plugin_Handled;
    }
	
    char arg[65];
    GetCmdArg(1, arg, sizeof(arg));
    char target_name[MAX_TARGET_LENGTH];
    int target_list[MAXPLAYERS], target_count;
    bool tn_is_ml;

    if ((target_count = ProcessTargetString(
            arg,
            client,
            target_list,
            MAXPLAYERS,
            COMMAND_FILTER_NO_IMMUNITY|COMMAND_FILTER_ALIVE|COMMAND_FILTER_NO_MULTI,
            target_name,
            sizeof(target_name),
            tn_is_ml)) <= 0)
    {
        ReplyToTargetError(client, target_count);
        return Plugin_Handled;
    }

    ForcePlayerSuicide(client);
    ChangeClientTeam(client, 1);

    for (int i = 0; i < target_count; i++)
    {
        if (target_list[i] != client && IsClientConnected(target_list[i]))
        {
            Client_SetObserverMode(client, OBS_MODE_IN_EYE, true);
            Client_SetObserverTarget(client, target_list[i]);
            // SetClientViewEntity(client, target_list[i]);
        }
    }

    return Plugin_Handled;
}
