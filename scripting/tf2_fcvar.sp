//Includes
#include <sourcemod>
#include <updater>
#include <morecolors>

#pragma newdecls required

#define UPDATE_URL    "https://bitbucket.org/jsza/tempus-sourcemod-plugins/raw/master/tf2_fcvar.txt"
#define PLUGIN_VERSION          "1.0.1"

public Plugin myinfo = {
    name        = "TF2 Fcvar",
    author      = "Tempus",
    version     = PLUGIN_VERSION,
};

bool g_bChangeIsNeeded[MAXPLAYERS];
bool g_bNotified[MAXPLAYERS];
bool g_bSpawned[MAXPLAYERS];

public void OnClientDisconnect(int client){
	g_bChangeIsNeeded[client] = false;
	g_bNotified[client] = false;
	g_bSpawned[client] = false;
}

public void OnPluginStart()
{
	HookEvent("player_spawn", Event_player_spawn);
}

public void OnAllPluginsLoaded()
{
    if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
    RegServerCmd("sm_tempus_plugins_update", cmdPluginUpdate);
}

public Action cmdPluginUpdate(int args)
{
    if (LibraryExists("updater"))
    {
        Updater_ForceUpdate();
    }
}

public int Updater_OnPluginUpdated()
{

    if (LibraryExists("updater"))
    {
        ReloadPlugin();
    }
}

public void Event_player_spawn(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"))
	if (IsClientInGame(client) && !IsFakeClient(client))
	{
        char status[1];
        GetClientInfo(client, "sv_cheats", status, sizeof(status));
        if (StringToInt(status) == 0)
        {
            SendConVarValue(client, FindConVar("sv_cheats"), "1")
            SendConVarValue(client, FindConVar("sv_allow_wait_command"), "0")
        }
	}
	//Check clients interp and fov settings, and recommend changes
	if (!g_bNotified[client]){
		//Fix to prevent double spam on original spawn
		if (g_bSpawned[client]){
			int fov_default = GetEntProp(client, Prop_Send, "m_iDefaultFOV");
			float interp = GetEntPropFloat(client, Prop_Data, "m_fLerpTime");
			char sChanges[64];	
			
			if (interp >= 0.1){
				g_bChangeIsNeeded[client] = true;
				sChanges = "cl_interp 0; cl_interp_ratio 1;";
			}
			if (fov_default < 90){
				g_bChangeIsNeeded[client] = true;
				interp >= 0.1 ? (sChanges = "cl_interp 0; cl_interp_ratio 1; fov_desired 90;"): (sChanges = "fov_desired 90;");
			}	
			if (g_bChangeIsNeeded[client]){
				CPrintToChat(client, "{orange}Sub-optimal client settings detected\nRecommended changes:\n\n{white}%s\n\n{lightskyblue}Enter the above string into console", sChanges);
				if (interp >= 0.1){
					CPrintToChat(client, "{lightskyblue}Join spectator before applying interp changes");
				}
				g_bNotified[client] = true;
			}		
		}
		g_bSpawned[client] = true;
	}
}