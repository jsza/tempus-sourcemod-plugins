#include <sourcemod>

#pragma semicolon 1
#pragma newdecls required

#define IDLE_DELAY 300.0
#define VERSION "0.2"

public Plugin myinfo =
{
    name = "tempus_keypress",
    author = "sega74rus (modified by jayess)",
    description = "fire event key press SM to SP",
    version = VERSION,
    url = ""
}

bool inUse[MAXPLAYERS+1];
bool inJump[MAXPLAYERS+1];
bool inDuck[MAXPLAYERS+1];
bool inForward[MAXPLAYERS+1];
bool inAttack[MAXPLAYERS+1];
bool inAttack2[MAXPLAYERS+1];
bool inBack[MAXPLAYERS+1];
bool inLeft[MAXPLAYERS+1];
bool inRight[MAXPLAYERS+1];
bool inMoveleft[MAXPLAYERS+1];
bool inMoveright[MAXPLAYERS+1];
bool inReload[MAXPLAYERS+1];
bool inScore[MAXPLAYERS+1];

Handle idleTimers[MAXPLAYERS+1];
bool clientIdle[MAXPLAYERS+1];


public void OnPluginStart()
{
    HookEvent("player_say", eventPlayerSay);
}


public void OnClientPutInServer(int client)
{
    inUse[client] = false;
    inJump[client] = false;
    inDuck[client] = false;
    inForward[client] = false;
    inAttack[client] = false;
    inAttack2[client] = false;
    inLeft[client] = false;
    inRight[client] = false;
    inMoveleft[client] = false;
    inMoveright[client] = false;
    inReload[client] = false;
    inScore[client] = false;
    ResetIdle(client);
}


public void OnClientDisconnect(int client)
{
    Handle timer = idleTimers[client];
    if (timer != null)
        KillTimer(timer);
    idleTimers[client] = null;
    clientIdle[client] = false;
}


public Action eventPlayerSay(Event event, char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
    ResetIdle(client);
    return Plugin_Continue;
}


public Action OnClientCommand(int client, int args)
{
    ResetIdle(client);
    return Plugin_Continue;
}


public void OnGameFrame()
{

    for (int client = 1; client <= MaxClients; client++)
    {
        if (IsClientInGame(client))
        {

            //fireIfChanged(client, "IN_USE", IN_USE, inUse);
            fireIfChanged(client, "IN_JUMP", IN_JUMP, inJump);
            fireIfChanged(client, "IN_DUCK", IN_DUCK, inDuck);
            fireIfChanged(client, "IN_FORWARD", IN_FORWARD, inForward);
            fireIfChanged(client, "IN_ATTACK", IN_ATTACK, inAttack);
            fireIfChanged(client, "IN_ATTACK2", IN_ATTACK2, inAttack2);
            fireIfChanged(client, "IN_BACK", IN_BACK, inBack);
            //fireIfChanged(client, "IN_LEFT", IN_LEFT, inLeft);
            //fireIfChanged(client, "IN_RIGHT", IN_RIGHT, inRight);
            fireIfChanged(client, "IN_MOVELEFT", IN_MOVELEFT, inMoveleft);
            fireIfChanged(client, "IN_MOVERIGHT", IN_MOVERIGHT, inMoveright);
            //fireIfChanged(client, "IN_RELOAD", IN_RELOAD, inReload);
            fireIfChanged(client, "IN_SCORE", IN_SCORE, inScore);
        }
    }
}


void fireIfChanged(int client, char command[15], int button, bool[] inArray)
{
    int buttons = GetClientButtons(client);
    bool pressing = (buttons & button > 0);
    bool wasPressing = inArray[client];

    inArray[client] = pressing;
    if (wasPressing != pressing) {
        Event event = CreateEvent("tempus_keypress", true);
        event.SetInt("userid", GetClientUserId(client));
        event.SetString("command", command);
        event.SetBool("status", pressing);
        event.Fire();

        ResetIdle(client);
    }
}


void ResetIdle(int client)
{
    Handle oldTimer = idleTimers[client];
    if (oldTimer != null)
        KillTimer(oldTimer);

    Handle timer = CreateTimer(IDLE_DELAY, SetIdle, client);
    idleTimers[client] = timer;

    if (clientIdle[client])
    {
        clientIdle[client] = false;
        FireIdle(client, false);
    }
}


public Action SetIdle(Handle timer, any client)
{
    clientIdle[client] = true;
    FireIdle(client, true);
    idleTimers[client] = null;
}

void FireIdle(int client, bool state)
{
    if (ClientValid(client)){
        Event event = CreateEvent("tempus_idle");
        if (event != null)
        {
            event.SetInt("userid", GetClientUserId(client));
            event.SetBool("state", state);
            event.Fire();
        }
	}
}

public bool ClientValid(int client){
	if (client < 1){
		return false;
	}
	if (IsClientConnected(client) && IsClientInGame(client) && !IsClientSourceTV(client)){
		return true;
	}
	else{
		return false;
	}
}
