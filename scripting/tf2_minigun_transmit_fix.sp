#include <sourcemod>
#include <sdktools>
#include <updater>

#pragma newdecls required

#define UPDATE_URL    "https://bitbucket.org/jsza/tempus-sourcemod-plugins/raw/master/tf2_minigun_transmit_fix.txt"
#define PLUGIN_VERSION          "1.0.2"

public Plugin myinfo = {
    name        = "TF2 Minigun Transmit Fix",
    author      = "Tempus",
    version     = PLUGIN_VERSION,
};

public void OnAllPluginsLoaded()
{
    if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
    RegServerCmd("sm_tempus_plugins_update", cmdPluginUpdate);
}

public Action cmdPluginUpdate(int args)
{
    if (LibraryExists("updater"))
    {
        Updater_ForceUpdate();
    }
}

public int Updater_OnPluginUpdated()
{

    if (LibraryExists("updater"))
    {
        ReloadPlugin();
    }
}

// This is the main 'event trigger' for the plugin when stuff starts happening
public void OnEntityCreated(int entity, const char[] classname)
{
    // The algorithm must be done next frame so that the entity is fully spawned
    CreateTimer(0.0, ProcessEdict, entity, TIMER_FLAG_NO_MAPCHANGE);

    return;
}

public Action ProcessEdict(Handle timer, any edict)
{
    if (!IsValidEdict(edict))
    {
        return Plugin_Handled;
    }

    char netclassname[64];
    GetEntityNetClass(edict, netclassname, sizeof(netclassname));

    if (FindSendPropInfo(netclassname, "m_iItemDefinitionIndex") == -1)
        return Plugin_Handled;

    char classname[64];
    GetEntityClassname(edict, classname, sizeof(classname));

    if ((StrEqual(classname, "tf_weapon_minigun")) || (StrEqual(classname, "tf_weapon_flamethrower")) || (StrEqual(classname, "tf_weapon_rocketlauncher_fireball")) || (StrEqual(classname, "obj_attachment_sapper")) || (StrEqual(classname, "tf_weapon_sapper"))) {
        RemoveEdict(edict);
    }

    return Plugin_Handled;
}
