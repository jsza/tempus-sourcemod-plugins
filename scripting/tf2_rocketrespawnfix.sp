#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <tf2_stocks>
#include <updater>

#pragma semicolon 1
#pragma newdecls required

#define UPDATE_URL    "https://bitbucket.org/jsza/tempus-sourcemod-plugins/raw/master/tf2_rocketrespawnfix.txt"
#define PLUGIN_VERSION          "1.0.3"

public Plugin myinfo = {
    name        = "TF2 Rocket Respawn Fix",
    author      = "Tempus",
    version     = PLUGIN_VERSION,
};

public void OnPluginStart()
{
    HookEvent("tempus_zonetele", OnTempusZoneTele);
    HookEvent("player_spawn", OnPlayerSpawn);
    HookEvent("player_death", OnPlayerSpawn);
}

public void OnAllPluginsLoaded()
{
    if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
    RegServerCmd("sm_tempus_plugins_update", cmdPluginUpdate);
}

public Action cmdPluginUpdate(int args)
{
    if (LibraryExists("updater"))
    {
        Updater_ForceUpdate();
    }
}

public int Updater_OnPluginUpdated()
{

    if (LibraryExists("updater"))
    {
        ReloadPlugin();
    }
}

public Action OnTempusZoneTele(Handle event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
    if (TF2_GetPlayerClass(client) == TFClass_DemoMan) {
        SetEntityHealth(client, 175);
        DestroyProjectilesDemo(client);
    } else if (TF2_GetPlayerClass(client) == TFClass_Soldier) {
        DestroyProjectilesSoldier(client);
    }
    SetEntPropFloat(client, Prop_Data, "m_flGravity", 1.0);
    return Plugin_Continue;
}

public Action OnPlayerSpawn(Handle event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
    DestroyProjectilesSoldier(client);
    SetEntPropFloat(client, Prop_Data, "m_flGravity", 1.0);
    return Plugin_Continue;
}

public void DestroyProjectilesDemo(int client)
{
    if (!IsValidEntity(client))
    {
        return;
    }

    int entity = -1;
    
    // Stickies 
    while ((entity = FindEntityByClassname(entity, "tf_projectile_pipe_remote")) != -1)
    {
        if (IsValidEntity(entity))
        {
            // Uses different ent property for tracking the owner then Soldier
            if (GetEntPropEnt(entity, Prop_Data, "m_hThrower") == client)
            {
                CreateTimer(0.0, DestroyProjectile, entity, TIMER_FLAG_NO_MAPCHANGE);
            }
        }
    }
    
    // Pipes
    while ((entity = FindEntityByClassname(entity, "tf_projectile_pipe")) != -1)
    {
        if (IsValidEntity(entity))
        {
            // Uses different ent property for tracking the owner then Soldier
            if (GetEntPropEnt(entity, Prop_Data, "m_hThrower") == client)
            {
                CreateTimer(0.0, DestroyProjectile, entity, TIMER_FLAG_NO_MAPCHANGE);
            }
        }
    }  
}

public void DestroyProjectilesSoldier(int client)
{
    if (!IsValidEntity(client))
    {
        return;
    }

    int entity = -1;
    
    // Normal Rockets
    while ((entity = FindEntityByClassname(entity, "tf_projectile_rocket")) != -1)
    {
        if (IsValidEntity(entity))
        {
            if (GetEntPropEnt(entity, Prop_Data, "m_hOwnerEntity") == client)
            {
                CreateTimer(0.0, DestroyProjectile, entity, TIMER_FLAG_NO_MAPCHANGE);
            }
        }
    }

    // Cow Mangler rockets
    while ((entity = FindEntityByClassname(entity, "tf_projectile_energy_ball")) != -1)
    {
        if (IsValidEntity(entity))
        {
            if (GetEntPropEnt(entity, Prop_Data, "m_hOwnerEntity") == client)
            {
                CreateTimer(0.0, DestroyProjectile, entity, TIMER_FLAG_NO_MAPCHANGE);
            }
        }
    }
}

public Action DestroyProjectile(Handle timer, any edict)
{
    if (IsValidEntity(edict))
    {
        RemoveEdict(edict);
    }
}
