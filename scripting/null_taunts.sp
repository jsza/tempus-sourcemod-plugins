#include <tf2>
#include <tf2_stocks>

public Action OnPlayerRunCmd(int client) 
{
    if (TF2_IsPlayerInCondition(client, TFCond_Taunting) && !(GetEntityFlags(client) & FL_ONGROUND)) 
    {
        TF2_RemoveCondition(client, TFCond_Taunting);
    }
}