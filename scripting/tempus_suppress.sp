#include <sourcemod>
#include <morecolors>

#pragma semicolon 1
#pragma newdecls required

char previousJoinName[MAX_NAME_LENGTH];
char previousLeaveName[MAX_NAME_LENGTH];

public void OnPluginStart()
{
    HookEvent("player_team", ev_PlayerTeam, EventHookMode_Pre);
    HookEvent("player_disconnect", ev_PlayerDisconnect, EventHookMode_Pre);
    HookEvent("player_connect", ev_PlayerConnect, EventHookMode_Pre);
	HookEvent("player_connect_client", ev_PlayerConnectClient, EventHookMode_Pre);
    HookEvent("player_death", ev_PlayerDeath, EventHookMode_Pre);
}

public Action ev_PlayerTeam(Handle event, const char[] name, bool dontBroadcast)
{
    SetEventBroadcast(event, true);
    return Plugin_Continue;
}

public Action ev_PlayerDisconnect(Handle event, const char[] name, bool dontBroadcast)
{
    char strName[MAX_NAME_LENGTH];
    GetEventString(event, "name", strName, sizeof(strName));
    if (!StrEqual(strName, previousLeaveName)) {
        CPrintToChatAll("{lightskyblue}%s {white}has left the server.", strName);
    }
    previousLeaveName = strName;
    SetEventBroadcast(event, true);
    return Plugin_Continue;
}

public Action ev_PlayerConnect(Handle event, const char[] name, bool dontBroadcast)
{
    SetEventBroadcast(event, true);
    return Plugin_Continue;
}

public Action ev_PlayerConnectClient(Handle event, const char[] name, bool dontBroadcast)
{
	char strName[MAX_NAME_LENGTH];
    GetEventString(event, "name", strName, sizeof(strName));
    if (!StrEqual(strName, previousJoinName)) {
        CPrintToChatAll("{lightskyblue}%s {white}is joining the server.", strName);
    }
    previousJoinName = strName;
    SetEventBroadcast(event, true);
    return Plugin_Continue;
}

public Action ev_PlayerDeath(Handle event, const char[] name, bool dontBroadcast)
{
	SetEventBroadcast(event, true);
	return Plugin_Continue;
}
