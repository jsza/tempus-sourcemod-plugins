#include <sourcemod>

#pragma semicolon 1
#pragma newdecls required
#define PLUGIN_VERSION "1.0.0"

public Plugin myinfo = {
    name = "Speedometer",
    author = "CrancK (modified for Tempus)",
    description = "Speedometer",
    version = PLUGIN_VERSION,
    url = ""
};

bool speedo[MAXPLAYERS];
Handle SpeedOMeter;

int g_iClientSpeedoColour[MAXPLAYERS];
int g_iClientSpeedoColourCustom[MAXPLAYERS][3];

#define SPEEDO_NORMAL 0
#define SPEEDO_BLUE 1
#define SPEEDO_LIGHT 2
#define SPEEDO_DARK 3
#define SPEEDO_GREEN 4
#define SPEEDO_RED 5
#define SPEEDO_CUSTOM 6
#define SPEEDO_RAINBOW 7



public void OnPluginStart()
{
	CreateConVar("sm_speedo_version", PLUGIN_VERSION, "Speedometer Version", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	RegConsoleCmd("sm_speedo", Command_Speedo);
	RegConsoleCmd("sm_speedoblue", Speedo_Blue);
	RegConsoleCmd("sm_speedolight", Speedo_Light);
	RegConsoleCmd("sm_speedodark", Speedo_Dark);
	RegConsoleCmd("sm_speedogreen", Speedo_Green);
	RegConsoleCmd("sm_speedored", Speedo_Red);
	RegConsoleCmd("sm_speedocustom", Speedo_Custom);
	RegConsoleCmd("sm_speedorainbow", Speedo_Rainbow);

	SpeedOMeter = CreateHudSynchronizer();
}

public void OnClientPutInServer(int client){
	speedo[client] = false;
	g_iClientSpeedoColour[client] = SPEEDO_NORMAL;
	g_iClientSpeedoColourCustom[client][0] = 255;
	g_iClientSpeedoColourCustom[client][1] = 50;
	g_iClientSpeedoColourCustom[client][2] = 50;
}

public Action Command_Speedo(int client, int args)
{
	if(!speedo[client]){
		speedo[client] = true; 
		ReplyToCommand(client, "Speedo ON"); 
	}
	else{
		speedo[client] = false;
		ReplyToCommand(client, "Speedo OFF");
	}		
	return Plugin_Handled;
}

public Action Speedo_Blue(int client, int args)
{
	if (g_iClientSpeedoColour[client] == SPEEDO_BLUE)
	{
	     g_iClientSpeedoColour[client] = SPEEDO_NORMAL;
	     ReplyToCommand(client, "Speedo is default");
	}
	else{
	     g_iClientSpeedoColour[client] = SPEEDO_BLUE;
	     ReplyToCommand(client, "Speedo is blue");
	}
	return Plugin_Handled;
}

public Action Speedo_Light(int client, int args)
{
	if (g_iClientSpeedoColour[client] == SPEEDO_LIGHT)
	{
	     g_iClientSpeedoColour[client] = SPEEDO_NORMAL;
	     ReplyToCommand(client, "Speedo is default");
	}	
	else{
	     g_iClientSpeedoColour[client] = SPEEDO_LIGHT;
	     ReplyToCommand(client, "Speedo is lighter");
	}
	return Plugin_Handled;
}

public Action Speedo_Dark(int client, int args)
{
	if (g_iClientSpeedoColour[client] == SPEEDO_DARK)
	{
	     g_iClientSpeedoColour[client] = SPEEDO_NORMAL;
	     ReplyToCommand(client, "Speedo is default");
	}
	else{
	     g_iClientSpeedoColour[client] = SPEEDO_DARK;
	     ReplyToCommand(client, "Speedo is darker");
	}
	return Plugin_Handled;
}

public Action Speedo_Green(int client, int args)
{
	if (g_iClientSpeedoColour[client] == SPEEDO_GREEN)
	{
	     g_iClientSpeedoColour[client] = SPEEDO_NORMAL;
	     ReplyToCommand(client, "Speedo is default");
	}
	else{
	     g_iClientSpeedoColour[client] = SPEEDO_GREEN;
	     ReplyToCommand(client, "Speedo is green");
	}
	return Plugin_Handled;
}

public Action Speedo_Red(int client, int args)
{
	if (g_iClientSpeedoColour[client] == SPEEDO_RED)
	{
	     g_iClientSpeedoColour[client] = SPEEDO_NORMAL;
	     ReplyToCommand(client, "Speedo is default");
	}
	else{
	     g_iClientSpeedoColour[client] = SPEEDO_RED;
	     ReplyToCommand(client, "Speedo is red");
	}
	return Plugin_Handled;
}

public Action Speedo_Custom(int client, int args)
{
	if (args < 3)
	{
	ReplyToCommand(client, "Please enter three numbers");	
	return Plugin_Handled;
	}
	
	bool bRedZeroCheck = true;
	bool bGreenZeroCheck = true;
	bool bBlueZeroCheck = true;
	
	char arg1[4];
	char arg2[4];
	char arg3[4];
	
	//char sInputCheck[3][3];
	
	GetCmdArg(1, arg1, sizeof(arg1));
	GetCmdArg(2, arg2, sizeof(arg2));
	GetCmdArg(3, arg3, sizeof(arg3));
	
	/*for (int i = 0; i > 3; i++){
	sInputCheck[i][i] = arg1[i];
	sInputCheck[i][i] = arg2[i];
	sInputCheck[i][i] = arg3[i];
	}
	*/
	
	if (!StrEqual(arg1[0], "0")){
	bRedZeroCheck = false;
	}
	if (!StrEqual(arg2[0], "0")){
	bGreenZeroCheck = false;
	}
	if (!StrEqual(arg3[0], "0")){
	bBlueZeroCheck = false;
	}
	
	int red = StringToInt(arg1);
	int green = StringToInt(arg2);
	int blue = StringToInt(arg3);
	
	//Print for testing
	//ReplyToCommand(client, "arg1: %s, arg2: %s, arg3: %s || red: %d, green: %d, blue: %d red b: %b green b: %b blue //b: %b", arg1, arg2, arg3, red, green, blue, bRedZeroCheck, bGreenZeroCheck, bBlueZeroCheck);
	
	if ((red == 0 && bRedZeroCheck == false)
	|| (green == 0 && bGreenZeroCheck == false)
	|| (blue == 0 && bBlueZeroCheck == false))
	{
		ReplyToCommand(client, "Please enter in numbers");	
		return Plugin_Handled;
	}
	
	if ((red >=0 && red <=255) && (green >=0 && green <=255) && (blue >=0 && blue <=255))
	{
		g_iClientSpeedoColour[client] = SPEEDO_CUSTOM;
		ReplyToCommand(client, "Speedo is now (%d, %d, %d)", red, green, blue);
		g_iClientSpeedoColourCustom[client][0] = red;
		g_iClientSpeedoColourCustom[client][1] = green;
		g_iClientSpeedoColourCustom[client][2] = blue;
		return Plugin_Handled;
	}
	else {
		ReplyToCommand(client, "Please enter numbers between 0 and 255");
		return Plugin_Handled;
	}	
}

public Action Speedo_Rainbow(int client, int args)
{
	if (g_iClientSpeedoColour[client] == SPEEDO_RAINBOW)
	{
	     g_iClientSpeedoColour[client] = SPEEDO_NORMAL;
	     ReplyToCommand(client, "Speedo is default");
	}
	else{
	     g_iClientSpeedoColour[client] = SPEEDO_RAINBOW;
	     ReplyToCommand(client, "Speedo is fabulous");
	}
	return Plugin_Handled;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
	float currentVel[3], currentSpd;
	
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", currentVel);
	currentSpd = SquareRoot( (currentVel[0]*currentVel[0]) + (currentVel[1]*currentVel[1]) );
	
	if(speedo[client])
		{
		if (g_iClientSpeedoColour[client] == SPEEDO_BLUE){
			SetHudTextParams(0.44, 0.67, 1.0, 50, 100, 200, 255);
		}
		else if (g_iClientSpeedoColour[client] == SPEEDO_LIGHT){
			SetHudTextParams(0.44, 0.67, 1.0, 255, 255, 255, 255);
		}
		else if (g_iClientSpeedoColour[client] == SPEEDO_DARK){
			SetHudTextParams(0.44, 0.67, 1.0, 25, 25, 25, 255);
		}
		else if (g_iClientSpeedoColour[client] == SPEEDO_GREEN){
			SetHudTextParams(0.44, 0.67, 1.0, 50, 200, 0, 255);
		}
		else if (g_iClientSpeedoColour[client] == SPEEDO_RED){
			SetHudTextParams(0.44, 0.67, 1.0, 255, 0, 0, 255);
		}
		else if (g_iClientSpeedoColour[client] == SPEEDO_CUSTOM){
			SetHudTextParams(0.44, 0.67, 1.0, 
			g_iClientSpeedoColourCustom[client][0],
			g_iClientSpeedoColourCustom[client][1], 
			g_iClientSpeedoColourCustom[client][2], 255);
		}
		else if (g_iClientSpeedoColour[client] == SPEEDO_RAINBOW){
			int iRandRedBool = GetRandomInt(0,1);
			int iRandGreenBool = GetRandomInt(0,1);
			int iRandBlueBool = GetRandomInt(0,1);
				
			if (iRandRedBool){
			g_iClientSpeedoColourCustom[client][0]++;
			}
			else{
			g_iClientSpeedoColourCustom[client][0]--;
			}
			
			if (iRandGreenBool){
			g_iClientSpeedoColourCustom[client][1]++;
			}
			else{
			g_iClientSpeedoColourCustom[client][1]--;
			}
				
			if (iRandBlueBool){
			g_iClientSpeedoColourCustom[client][2]++;
			}
			else{
			g_iClientSpeedoColourCustom[client][2]--;
			}
			
			SetHudTextParams(0.44, 0.67, 1.0, 
			g_iClientSpeedoColourCustom[client][0],
			g_iClientSpeedoColourCustom[client][1], 
			g_iClientSpeedoColourCustom[client][2], 255, 0);
		}
		else {
		SetHudTextParams(0.44, 0.67, 1.0, 255, 50, 50, 255);
		}
		ShowSyncHudText(client, SpeedOMeter, "       %.0f u/s", currentSpd);    
	}    
}