#include <sourcemod>
#include <tf2>
#include <tf2_stocks>
#include <updater>

#pragma newdecls required
#pragma semicolon 1


#define UPDATE_URL    "https://bitbucket.org/jsza/tempus-sourcemod-plugins/raw/master/tf2_owntele.txt"
#define PLUGIN_VERSION          "1.0.0"

public Plugin myinfo = {
    name        = "TF2 Own Tele",
    author      = "Tempus",
    version     = PLUGIN_VERSION,
};

int g_Teleporters[MAXPLAYERS+1] = {-1, ...};

public void OnPluginStart()
{
    HookEvent("player_spawn", OnPlayerSpawn);
    HookEvent("player_builtobject", OnObjectBuilt);
    HookEvent("object_destroyed", OnObjectDestroyed);
}

public void OnAllPluginsLoaded()
{
    if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
    RegServerCmd("sm_tempus_plugins_update", cmdPluginUpdate);
}

public Action cmdPluginUpdate(int args)
{
    if (LibraryExists("updater"))
    {
        Updater_ForceUpdate();
    }
}

public int Updater_OnPluginUpdated()
{

    if (LibraryExists("updater"))
    {
        ReloadPlugin();
    }
}

public void OnClientPutInServer(int client)
{
    g_Teleporters[client] = -1;
}

public Action OnPlayerSpawn(Handle event, char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
    g_Teleporters[client] = -1;
}

public Action OnObjectBuilt(Handle event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
    int index = GetEventInt(event, "index");
    int objectType = GetEventInt(event, "object");

    if (objectType == 1)
    {
        if (TF2_GetObjectMode(index) == TFObjectMode_Entrance)
        {
            g_Teleporters[client] = index;
        }
    }
}

public Action OnObjectDestroyed(Handle event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
    int index = GetEventInt(event, "index");
    int objectType = GetEventInt(event, "object");

    if (objectType == 1)
    {
        if (TF2_GetObjectMode(index) == TFObjectMode_Entrance)
        {
            g_Teleporters[client] = -1;
        }
    }
}

public Action TF2_OnPlayerTeleport(int client, int teleporter, bool &result)
{
    if (teleporter == g_Teleporters[client])
    {
        return Plugin_Continue;
    }
    result = false;
    return Plugin_Changed;
}
