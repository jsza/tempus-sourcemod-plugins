// Code from Jump Assist

#include <sourcemod>
#include <sdktools>
#include <updater>

#pragma newdecls required

#define UPDATE_URL    "https://bitbucket.org/jsza/tempus-sourcemod-plugins/raw/master/tf2_lockcps.txt"
#define PLUGIN_VERSION          "1.0.0"

public Plugin myinfo = {
    name        = "TF2 Lock CPs",
    author      = "Tempus",
    version     = PLUGIN_VERSION,
};


public void OnPluginStart()
{
	HookEvent("teamplay_round_start", eventRoundStart);
}

public void OnAllPluginsLoaded()
{
    if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
    RegServerCmd("sm_tempus_plugins_update", cmdPluginUpdate);
}

public Action cmdPluginUpdate(int args)
{
    if (LibraryExists("updater"))
    {
        Updater_ForceUpdate();
    }
}

public int Updater_OnPluginUpdated()
{

    if (LibraryExists("updater"))
    {
        ReloadPlugin();
    }
}

public Action eventRoundStart(Handle event, const char[] name, bool dontBroadcast)
{
	LockCPs();
}

void LockCPs()
{
	int iCP = -1;
	while ((iCP = FindEntityByClassname(iCP, "trigger_capture_area")) != -1)
	{
		SetVariantString("2 0");
		AcceptEntityInput(iCP, "SetTeamCanCap");
		SetVariantString("3 0");
		AcceptEntityInput(iCP, "SetTeamCanCap");
	}
}
