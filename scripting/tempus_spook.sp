#include sdktools
#include morecolors
#include tf2_stocks

#pragma newdecls required

bool g_bSpook[MAXPLAYERS];
bool g_bForced = false;

char rare[][] = {"models/player/items/taunts/yeti_standee/yeti_standee.mdl",
                "models/player/items/taunts/yeti/yeti.mdl",
                "models/bots/headless_hatman.mdl"};
                    
#define SpookerModel "models/bots/skeleton_sniper/skeleton_sniper.mdl"
#define SoldierModel "models/player/soldier.mdl"
#define DemoModel "models/player/demo.mdl"


public void OnPluginStart() {
    RegConsoleCmd("sm_spook", cSpook);
    PrecacheModel("models/bots/skeleton_sniper/skeleton_sniper.mdl", true);
    HookEvent("player_spawn", eventSpawn, EventHookMode_Post);
    HookEvent("tempus_holiday", eventHoliday, EventHookMode_Post);
}

public void OnClientConnected(int client){
    g_bSpook[client] = g_bForced ? true : false;
}

public void eventHoliday(Event event, const char[] name, bool dontBroadcast){    
    char type[32];
    GetEventString(event, "holiday", type, sizeof(type), "");
    if (StrEqual(type, "halloween")) {
        g_bForced = true;
        setArrayBool(true);
    } else {
        g_bForced = false;
        setArrayBool(false);
    }
}

public void setArrayBool(bool b) {
    for (int i=0; i < MAXPLAYERS; i++) {
        g_bSpook[i] = b;
    }
}

public void eventSpawn(Event event, const char[] name, bool dontBroadcast){    
    int client = GetClientOfUserId(GetEventInt(event, "userid"))
    if (g_bSpook[client]){
        if (IsClientInGame(client)){
           Conscript(client);
        }
    }
}


public Action cSpook(int client, int args) {
    CReplyToCommand(client, "{haunted}~Spook~ {white}%s", (g_bSpook[client] ? (g_bSpook[client] = false) : (g_bSpook[client] = true)) ? "enabled on respawn" : "disabled");
    if (TF2_GetPlayerClass(client) == TF2_GetClass("soldier")){
        SetVariantString(SoldierModel);
    }
    else if (TF2_GetPlayerClass(client) == TF2_GetClass("demoman")){
        SetVariantString(DemoModel);
    }
    AcceptEntityInput(client, "SetCustomModel");
    SetEntProp(client, Prop_Send, "m_bUseClassAnimations", 1);
    SetEntityRenderColor(client, 255, 255, 255, 255);
    return Plugin_Handled;
}

public void Conscript(int client) {
    int red, green, blue;
    red = GetRandomInt(0, 255);
    green = GetRandomInt(0, 255);
    blue = GetRandomInt(0, 255);
    
    SetVariantString(SpookerModel);
    
    if (GetRandomInt(0, 15000) == 2020 && (g_bForced)) {
        int choice = GetRandomInt(0, 2);
        SetVariantString(rare[choice]);
    }  
    
    AcceptEntityInput(client, "SetCustomModel");
    SetEntProp(client, Prop_Send, "m_bUseClassAnimations", 1);
    SetEntityRenderColor(client, red, green, blue, 255);
}