#include <sourcemod>
#include <sdkhooks>
#include <updater>

#pragma newdecls required

#define UPDATE_URL    "https://bitbucket.org/jsza/tempus-sourcemod-plugins/raw/master/tf2_deleteroundtimer.txt"
#define PLUGIN_VERSION          "1.0.0"

public Plugin myinfo = {
    name        = "TF2 Delete Round Timer",
    author      = "Tempus",
    version     = PLUGIN_VERSION,
};

public void OnAllPluginsLoaded()
{
    if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
    RegServerCmd("sm_tempus_plugins_update", cmdPluginUpdate);
}

public Action cmdPluginUpdate(int args)
{
    if (LibraryExists("updater"))
    {
        Updater_ForceUpdate();
    }
}

public int Updater_OnPluginUpdated()
{
    if (LibraryExists("updater"))
    {
        ReloadPlugin();
    }
}

public void OnEntityCreated(int entity, const char[] classname)
{
    if (StrEqual(classname, "team_round_timer"))
    {
        SDKHook(entity, SDKHook_Spawn, OnRoundTimerSpawned);
    }

    return;
}

public Action OnRoundTimerSpawned(int entity)
{
    CreateTimer(0.0, RemoveRoundTimer, entity);
}

public Action RemoveRoundTimer(Handle timer, any edict)
{
    RemoveEdict(edict);
}
