#include <sourcemod>

#pragma semicolon 1
#pragma newdecls required

int g_bBhopEnabled[MAXPLAYERS+1];

public void OnPluginStart()
{
    HookEvent("tempus_autobhop", Event_tempus_autobhop);
}

public void OnClientPutInServer(int client)
{
    g_bBhopEnabled[client] = false;
}

public Action Event_tempus_autobhop(Handle event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
    bool bState = GetEventBool(event, "state");
    g_bBhopEnabled[client] = bState;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
    if (g_bBhopEnabled[client])
    {
        if (buttons & IN_JUMP)
        {
            if (!(GetEntityFlags(client) & FL_ONGROUND))
            {
                if (!(GetEntityMoveType(client) & MOVETYPE_LADDER))
                {
                    int iType = GetEntProp(client, Prop_Data, "m_nWaterLevel");
                    if (iType <= 1)
                    {
                        buttons &= ~IN_JUMP;
                    }
                }
            }
        }
    }
    return Plugin_Continue;
}
