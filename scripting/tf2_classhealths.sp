#include <sourcemod>
#include <tf2_stocks>
#include <sdkhooks>
#include <updater>

#define UPDATE_URL    "https://bitbucket.org/jsza/tempus-sourcemod-plugins/raw/master/tf2_classhealths.txt"
#define PLUGIN_VERSION          "1.0.2"
#define DMG_FALL   (1 << 5)

#pragma newdecls required

public Plugin myinfo = {
    name        = "TF2 Class Healths",
    author      = "Tempus",
    version     = PLUGIN_VERSION,
};

int g_maxHealth[10] = {0, 125, 125, 900, 175, 150, 300, 175, 125, 125};

public void OnAllPluginsLoaded()
{
    if (LibraryExists("updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
    RegServerCmd("sm_tempus_plugins_update", cmdPluginUpdate);
}

public Action cmdPluginUpdate(int args)
{
    if (LibraryExists("updater"))
    {
        Updater_ForceUpdate();
    }
}

public int Updater_OnPluginUpdated()
{

    if (LibraryExists("updater"))
    {
        ReloadPlugin();
    }
}

public void OnConfigsExecuted()
{
    for (int i = 1; i <= MaxClients; i++)
    {
        if (IsClientInGame(i))
        {
            SDKHook(i, SDKHook_GetMaxHealth, OnGetMaxHealth);
        }
    }
}

public void OnClientPutInServer(int client)
{
    SDKHook(client, SDKHook_GetMaxHealth, OnGetMaxHealth);
    SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}

public Action OnGetMaxHealth(int client, int &maxhealth)
{
    if (client > 0 && client <= MaxClients)
    {
        if (TF2_GetPlayerClass(client) == TFClass_Soldier)
        {
            maxhealth = g_maxHealth[TF2_GetPlayerClass(client)];
        }
        return Plugin_Handled;
    }
    return Plugin_Continue;
}

public Action OnTakeDamage(int client, int &attacker, int &inflictor, float &damage, int &damagetype)
{
    if (TF2_GetPlayerClass(client) == TFClass_Soldier && damagetype & DMG_FALL)
        return Plugin_Handled;
    return Plugin_Continue;
}
