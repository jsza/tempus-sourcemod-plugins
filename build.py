import os

from glob import glob

base_dir = os.getcwd()
spcomp_path = os.path.join(base_dir, 'spcomp.exe')
source_dir = os.path.join(base_dir, 'scripting')
build_dir = os.path.join(base_dir, 'plugins')
for filename in glob(os.path.join(source_dir, '*.sp')):
    cmd = '%s "%s" -o"%s"' % (spcomp_path, filename, filename.replace('scripting', 'plugins').replace('.sp', '.smx'))
    os.system(cmd)
